<?php

namespace App\Http\Controllers;

use App\Models\Obras;
use Illuminate\Http\Request;



class ObrasController extends Controller
{

public function index()
{
$data['obras'] = Obras::orderBy('id','desc')->paginate(5);
return view('obras.index', $data);
}

public function create()
{
return view('obras.create');
}

public function store(Request $request)
{
        
// $request->validate([
//         'name' => 'required',
//         'description' => 'required',
//         'construction_site' => 'required',
//         'work_type' => 'required',
//         'start' => 'required',
//         'budget' => 'required'	
// ]);
 
    $obra = new Obras;
    $obra->name = $request->name;
    $obra->description = $request->description;
    $obra->construction_site = $request->construction_site;
    $obra->work_type = $request->work_type;
    $obra->start = $request->start;
    $obra->budget = $request->budget;
// dd($request,$obra);
  
    $obra->save();
    return redirect()->route('obras.index')
    ->with('success','La obra se creo satisfactoriamente');

}

public function show(Obras $obra)
{
return view('obras.show',compact('obra'));
} 

public function edit(Obras $obra)
{
return view('obras.edit',compact('obra'));
}

public function update(Request $request, $id)
{
$request->validate([
        'name' => 'required',
        'description' => 'required',
        'construction_site' => 'required',
        'work_type' => 'required',
        'budget' => 'required',
        'end'=>'required'
]);
$obra = Obras::find($id);
$obra->name = $request->name;
$obra->description = $request->description;
$obra->construction_site = $request->construction_site;
$obra->work_type = $request->work_type;
$obra->budget = $request->budget;
$obra->end = $request->end;

$obra->save();
return redirect()->route('obras.index')
->with('success','La obra se actualizo satisfactoriamente');
}

public function destroy(Obras $obra)
{
$obra->delete();
return redirect()->route('obras.index')
->with('success','La obra se borro satisfactoriamente');
}
}
