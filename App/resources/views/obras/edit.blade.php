<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Editar</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
</head>

<body>
    <div class="container mt-2">
        <div class="row">
            <div class="col-lg-12 margin-tb">
                <div class="pull-left">
                    <h2>Editar Obra</h2>
                </div>
                <div class="pull-right">
                    <a class="btn btn-primary" href="{{ route('obras.index') }}"
                        enctype="multipart/form-data"> Regresar</a>
                </div>
            </div>
        </div>
        @if(session('status'))
            <div class="alert alert-success mb-1 mt-1">
                {{ session('status') }}
            </div>
        @endif
        <form action="{{ route('obras.update',$obra->id) }}" method="POST"
            enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <!-- Nombre de Obra -->
                        <strong>Nombre de obra:</strong>
                        <input type="text" name="name" value="{{ $obra->name }}" class="form-control"
                            placeholder="{{ $obra->name }}">
                        @error('name')
                            <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <!-- Descripcion -->
                        <strong>Descripcion:</strong>
                        <input type="text" name="description" value="{{ $obra->description }}" class="form-control"
                            placeholder="{{ $obra->description }}">
                        @error('description')
                            <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <!-- Localizacion -->
                        <strong>Localizacion:</strong>
                        <input type="text" name="construction_site" value="{{ $obra->construction_site }}" class="form-control"
                            placeholder="{{ $obra->construction_site }}">
                        @error('construction_site')
                            <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <!-- Tipo de obra -->
                        <strong>Tipo de obra:</strong>
                        <select name="work_type" value="{{$obra->work_type}}">
                            <option value="ELECTRICA"> ELECTRICA</option>
                            <option value="CIVIL"> CIVIL</option>
                        @error('work_type')
                            <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <!-- Inicio de obra -->
                        <strong>Inicio de obra:</strong>
                        <input type="date" name="start" value="{{ $obra->start }}" class="form-control"
                            placeholder="{{ $obra->start }}">
                        @error('start')
                            <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <!-- Final de obra -->
                        <strong>Final de obra:</strong>
                        <input type="date" name="end" value="{{ $obra->end }}" class="form-control"
                            placeholder="{{ $obra->end }}">
                        @error('end')
                            <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <!-- Presupuesto -->
                        <strong>Presupuesto:</strong>
                        <input type="text" name="budget" value="{{ $obra->budget }}" class="form-control"
                            placeholder="{{ $obra->budget }}">
                        @error('budget')
                            <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <button type="submit" class="btn btn-primary ml-3">Enviar</button>
            </div>
        </form>
    </div>
</body>

</html>