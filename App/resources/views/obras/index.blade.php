<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Laravel 8 CRUD Tutorial From Scratch</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
</head>

<body>
    <div class="container mt-2">
        <div class="row">
            <div class="col-lg-12 margin-tb">
                <div class="pull-left">
                    <h2>Obras</h2>
                </div>
                {{-- Boton para crar obras --}}
                <div class="pull-right mb-2">
                    <a class="btn btn-success" href="{{ route('obras.create') }}"> Crear Obra</a>
                </div>
            </div>
        </div>
        @if($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        <table class="table table-bordered">
            <tr>
                <th>S.No</th>
                <th>Nombre de obra</th>
                <th>Descripcion</th>
                <th>Localizacion</th>
                <th>Tipo de Obra</th>
                <th>Inicio de obra</th>
                <th>Final de obra</th>
                <th>presupuesto</th>

                
                <th width="280px">Action</th>
            </tr>
            @foreach($obras as $obra)
                <tr>
                    
                    <td>{{ $obra->id }}</td>
                    <td>{{ $obra->name }}</td>
                    <td>{{ $obra->description }}</td>
                    <td>{{ $obra->construction_site  }}</td>
                    <td>{{ $obra->work_type  }}</td>
                    <td>{{ $obra->start }}</td>
                    <td>{{ $obra->end  }}</td>
                    <td>{{ $obra->budget  }}</td>
                  
                    <td>
                        <form action="{{ route('obras.destroy',$obra->id) }}"
                            method="Post">
                            <a class="btn btn-primary"
                                href="{{ route('obras.edit',$obra->id) }}">Editar</a>
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger">Borrar</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </table>
        {!! $obras->links() !!}
</body>

</html>