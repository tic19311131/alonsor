<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Add Company Form - Laravel 8 CRUD</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
</head>

<body>
    <div class="container mt-2">
        <div class="row">
            <div class="col-lg-12 margin-tb">
                <div class="pull-left mb-2">
                    <!-- Nombre -->
                    <h2>Añadir Obra</h2>
                </div>
                <div class="pull-right">
                    <a class="btn btn-primary" href="{{ route('obras.index') }}">Volver</a>
                </div>
            </div>
        </div>
        @if(session('status'))
            <div class="alert alert-success mb-1 mt-1">
                {{ session('status') }}
            </div>
        @endif
        <form action="{{ route('obras.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <!-- Nombre de obra -->
                        <strong>Nombre de obra:</strong>
                        <input type="text" name="name" class="form-control" placeholder="Nombre de obra">
                        @error('name')
                            <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <!-- Descripcion -->
                        <strong>Descripcion:</strong>
                        <input type="text" name="description" class="form-control" placeholder="Descripcion">
                        @error('description')
                            <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <!-- Localizacion -->
                        <strong>Localizacion</strong>
                        <input type="text" name="construction_site" class="form-control" placeholder="Localizacion">
                        @error('contruction_site')
                            <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <!-- Tipo de Obra -->
                        <strong>Tipo de obra:</strong>
                        <select name="work_type" id="">
                            <option value="ELECTRICA"> ELECTRICA</option>
                            <option value="CIVIL"> CIVIL</option>
                        </select>
                        @error('work_type')
                            <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <!-- Inicio de Obra -->
                        <strong>Inicio de Obra:</strong>
                        <input type="date" name="start" class="form-control" placeholder="AAAA-MM-DD">
                        @error('start')
                            <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <!-- Final de obra -->
                        <strong>Final de Obra:</strong>
                        <input type="date" name="end" class="form-control" placeholder="AAAA-MM-DD">
                        @error('end')
                            <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <!-- Presupuesto -->
                        <strong>Presupuesto:</strong>
                        <input type="text" name="budget" class="form-control" placeholder="Presupuesto">
                        @error('budget')
                            <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <button type="submit" class="btn btn-primary ml-3">Submit</button>
            </div>
        </form>
</body>

</html>